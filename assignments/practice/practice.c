// Including all header files
#include  <stdio.h>//for standard input and output
#include <string.h>//for manipulating C strings and arrays
#include <stdlib.h>//for functions involving memory allocation,process control,conversions and others
#include <MQTTClient.h>//contains APIs to send/receive MQTT data
#include<time.h>//to get and manipulate date and time information
#include<netdb.h>//for network database operations
#include<ifaddrs.h>//getting network interface adderesses
#include <si/shunyaInterfaces.h>// to program ebedded boards to talk to PLCs,sensors,actuators,cloud.

//fetching device id
void getDeviceID() {
    char id[255];//variable declaration
    FILE *fp;
    fp = fopen("/etc/shunya/deviceid" ,"r"); // to read file
   
    while (fscanf(fp,"%s",id)!= EOF)
    {
       printf("%s",id)//print device id
    }
    
   
}

// Fetching timestamp
int getTimeStamp() {
     int ts;//variable declaration
     ts = (gettimeofday(2));// to get the time
     return ts;//returning the value
}

function getIPAdress(char *IP) {

 char *IP;//pointer
 IP = inet_ntoa(*((struct in_addr*)host_entry-->h_addr_list[0]));//value & conversion

 return 0;

}

//configuration of connections with AWS IOT core
void AWS_config(void){
"user":{
"endpoint": " ",
"port":8883,
"certificate dir":"/home/shunya/.cert/aws",
"root certificate":" ",
"client certificate":" ",
"private key":" ",
"client ID":" "
}
}

void get_Data(){
MQTTClient_message heartbeat = MQTTClient_message_intializer;//intializes message to send on broker
awsObj user = newAws("user");
awsConnectMqtt(&user);//connects to MQTT broker of AWS IOT core
heartbeat.payload("device":{
 "deviceId": id,
 "timestamp": ts,
 "eventType": "heartbeat",
 "ipAddress": IP
}.getBytes());//creates payload of message
awsPublishMqtt(&user, "device/heartbeat", "%s" ,heartbeat);
awsDisconnectMqtt(&user);
}


int  main(void) //main function
{
    //initLib();//intializing library
    shunyaInterface();// calling shunya interface
     getDeviceID();//calling device id function to fetch id
     getTimeStamp();// calling time stamp function
     getIPAdress();//calling IP address function to fetch IP address
     get_Data();
     AWS_config(void);
    
    
    return 0;

}







