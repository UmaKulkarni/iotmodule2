#include<stdio.h>//for standard input and output
#include<stdlib.h>//for functions involving memory allocation,process control,conversions and others
#include<string.h>//for manipulating C strings and arrays
#include <si/shunyaInterfaces.h>// to program ebedded boards to talk to PLCs,sensors,actuators,cloud.
#include<dirent.h>// format of directory entries

RIM = opendir("/dev/ttyAMA0");

int main(void){
shunyaInterfacesSetup(); // intialization of shunya interfaces

"modbus -tcp":{    //modbus TCP/IP configuration 
"type": "tcp",     // user enters data
"ipaddr": " ",
"port": " "}

//modbus connect r/w data
modbusObj RIM = newModbus("modbus-tcp");
//connect to modbus as per configuration
modbusTcpConnect(&RIM);

//reading information of parameters
float ac_current = modbusTcpRead(&RIM, 30007);//reading AC current
float voltage = modbusTcpRead(&RIM, 30001);//reading voltage
float power = modbusTcpRead(&RIM, 30013);//reading power
float active_energy = modbusTcpRead(&RIM, 30147);//reading Active energy
float reactive_energy = modbusTcpRead(&RIM, 30151);//reading Reactive energy
float apparent_energy = modbusTcpRead(&RIM, 30081);//reading Apparent energy
float THD_voltage = modbusTcpRead(&RIM, 30219);//reading THD of voltage

return 0;

}



