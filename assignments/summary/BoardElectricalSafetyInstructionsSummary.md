Do's and Don'ts:
1. Power supply--> Always output voltage of power supply matches input voltage
- While turning on circuit must be connected
- Don'ts--> do not connect power supply without matching power rating.
- never connect higher output to lower input
2. Handling-->while working keep circuiton flat wooden surface
- unplug deice before performing any action
- make sure hands are dry
- use external usb fan to make board cool
- Don'ts-->do not perform any action while plug is on
- do not touch the device when hands are wet
3. GPIO-->confirm whether board is running on 3.3 v or 5v
- connect LEDs with correct resistors
- use logic level converter whenever required
- Don'ts-->Never connect anything greater than 5v  to 3.3v
- do not pug anything with high voltage it can cause damage
- do not connect motor directly
![image](file:///home/user/Pictures/screenshot)
**Tips for UART, SPI,I2C**:
1.UART-->might require protection circuit
- UART used to communicate with the board through USB to TTL connection. It does not require protection circuit
- As per requirement use voltage divider circuit
- connect Rx pin of device1 to Tx pin of device2 and Tx of 1 to Rx of 2,
2. SPI--> development boards in push-pull mode
- does not require protection circuit
- generally 4.7kohm resistor is used.
but can use betwen 1-10kohm
- As per requirement we can use protection circuit along with pull up resistorson each th Slave select lines
3. I2C-->while using this SDA and SDL lines must be protected.
- protection can be done by using pull up registors on both lines
- 2.2 kOhm<= 4Kohm resistors are used.

