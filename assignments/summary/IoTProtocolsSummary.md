IIOT Protocols:
1. 4-20 mA--> standard protocol used
-current is same inside the loop
- voltages changes or drops in the loop.
> Components of 4-20mA current loop-
- **Sensor**-->measures process variable
-typically measures temperature,humidity,flow,level,or pressure.
- **Transmitter**-->converts sensor measurements into current signal
- **Power supply**-->supplies voltage
- must output a DC current i.e current flowing in one particular direction
- deciding what voltage to use,be sure that  power supply voltage must be 10% greater than total voltage drop of attached Components
-**Loop**-->wire connecting the sensor to device receiving 4-20mA signal am\nd then back to tramitter.
- **Receiver**--> receives and interprets cuurent signal
- digital displays,controllers,actuators and values are common devices to incorporate into loop.
- Advantages: -simplest option to connect and configure
- Less wiring and connections
- reduces initial costs
- better for long distances
- less sensitive to background noise
- simple to detect fault
- Disadvantages: -can get problems with ground loops
- can transit one particular process signal

<div align="center">![image](assignments/summary/assets/4-20ma.png)</div>


2. Modbus Communication Protocol:
- us with PLC 
- used to transmit signals from instrumentation and control devices back to main controller or data gathering systems
- device requesting information--> Master
- device supplying info--> slaves 
- standard network: 1 master and upto 247 slaves with unique adderesses
- communication between Master and slave in a frame--> Function code
- It identifies action to perform
- slaves responds based on Function code received
>commonly used as local interface to mange devices
> can be used over 2 interfaces-->RS485(Modbys RTU) and Ethernet(Modbus TCP/IP)
<div align="center">![image](assignments/summary/assets/modbus.png)</div>

3. OPCUA :
- client server based  communication
- server waits for clients request and responds accordingly
- Also can work as Pub-sub system.clients can subscribe to topics they want
- Client is very important, it decides when and what data should the server fetch from client
- Protocols--> -data access(DA)-for fetching data
- historical analysis(HA)- acess previous data and events
- Alarm and events(AE)-no stoarge after transferring
> uses symmetrical and asymmetrical encryption to secure Communication
- Unified architecture(UA)-->platform independent
- can access smallest part of complex data
- supports OOP
> Advantages:
- platform independent
- high scalability
- abstract base model
- object oriented
- internet firewall
- high performance

<div align="center">![image](assignments/summary/assets/opcua.png)</div>

**Cloud Protocols**:
1. MQTT-->
- Simultaneous Communication with many devices possible
- MQTT client as a publish sends messgse--> MQTT broker. Who distributes messages according to all other MQTT clients subscribed to topic on which it publishes Message.

<div align="center">![image](assignments/summary/assets/MQTT_ex.png)</div>
Applications:
<div align="center">![image](assignments/summary/assets/MQTT_apllications.png)</div>

2. HTTP -->
- Request has 3 parts:request line,HTTP header,message body
Response has 3 parts: status line, HTTP header, message body
-server sends different status codes at different situations e.g 200 ok,400 bad request,202 accepted etc. 

<div align="center">![image](assignments/summary/assets/http_work.png)</div>

**- Features/Characterstics:**
| MQTT                                                      | HTTP                                                       |
| Message Queuing Telemetry Transport-----------------------| Hyper Text Transport  Protocol-----------------------------|
| simple messaging protocol for devices with low  bandwidth | request response protocol                                  |
| Lightweight public subscribe system                       | clients sends HTTp request,server sends bach HTTP response |

