Sensors and Actuators:
- **Sensors**--> Also called as transducer.
- Converts one form of energy into another
- transducer converts some physical energy into an electrical signal, can be used to take reading
- e.g. Microphone sensor: converts vibrational energy --> electrical signals
> Types: Water level IOT sensor, Temperature IOT sensor,Light IOT sensor,Motion IOT sensor etc.

**Actuators**--> another type of transducer.


<div align="center">![image](assignments/summary/assets/Sensors_to_actuators_loop.png)</div>

- Operates gn **reverse direction** of Sensors
- takes electrical input ----> Physical output
- e.g. Electric Motor, hydraulic sysytem,pneumatic system
> Types:Linear Actuators,motors,relays,solenoids


**Analog and digital**:
signals are used to transmit data through electric signals 

Analog--> signal:continuous represented.
- waves: sine waves
- example: human voice in Air 
- application: Thermometer
Digital--> signal discreted time signals 
- waves: square
- example: P.Cs , CD, DVD
- applications: PCs , PDAs

<div align="center">![image](assignments/summary/assets/analog_digital.png)</div>



**Microprocessor and Microcontroller**:Microprocessor has strong I/O than controller.
- **Arduino UNO**: Microcontroller
-  Features: RAM-2k
- flash - 32k
- timers
- serial (UART)
- I2C 
- SPI
- no interpreter, O.S and Firmware

-**Raspberry Pi**:Microprocessor
- Single board computer
- Features: ethernet
- Arduino
- video
- USB(host)
- SD card
- HDMI
- GPIO
- 40 pin connector
- no storage
- System on chip(soc)
- can connect shields 
- Uses ARM
- can connect multiple pi together
- can connect to sensors and Actuators
> RPI interfaces:GPIO,UART,SPI,I2C,PWM
<div align="center">![image](assignments/summary/assets/Raspberry_pi.png)</div>

**Serial and Parallel communication**:
- parallel--> GPIO
- serial--> UART,SPI,I2C
<div align="center">![image](assignments/summary/assets/serial_parallel.png)</div>
